# Requirements for A Perfect Ticketing System

This document will serve as a collection of past experiences that have enabled overall success through a very significant tool within the business: The ticketing system.

# Preface

Over my career I have had a wide range of experiences with support tools. These experiences have proven invaluable in my understanding of what separates competing departments, starting with my time at Rackspace.

## CORE

Rackspace is well known for their dramatic attempts at self promotion. Over the years "fanatical support" has found its way into nearly every support department that wants to be successful in any way, even outside of our industry. It was frequently discussed that our performance goals were more dependant on satisfaction than monetary gain - if we did not exceed the customer's expectations in every interaction, it was a failure. "OK" was not acceptable.

It should not be unusual then to know that Rackspace went so far as to call their ticketing systme CORE. From its inception, Dirk Elmendorf knew from past call center experiences and the objectives he was taught in school, the methods for interacting with the customers were the most important part of being in customer service. As a result, that very paradigm became the core of the business. With exceptions in specific areas, CORE is still serving that very function today. I had a very personal interaction with CORE, as I was one of the primary administrators of the system.

At the beginning, CORE served in all forms:

* Accounting
* Inventry
* Scheduled Tasks/Maintenances
* Notifications
* Authentication
* Password Storage
* Customer Survey
* Analytics
* Support Knowledgebase
* Internal Permissions & SSO Access

In fact, before the company formally began organizing in a more enterprise manner, CORE even served as the LDAP service, directory, and domain controller.

All information was kept within the confines of the tool. It was extremely critical in its presentation of that information, as well.

### Engineered Success

CORE, initially, served as the portal that maintained all customer information, including payments, contracts, and other financial information. Integration was key in the system, and in any place we could we tied it together with another system, to enable employees faster access to what is necessary in their everyday customer interactions.

* Sales had access to phone numbers, contract information, limits, growth potential, past discussions, and where available company information (stock ticker, age, private/public, size, etc).
* Support had access to phone numbers, technical information at a glance for all datacenters, password information for servers, monitoring information, the uptime performance, the current/recent satisfaction state of the customer (and if necessary, the complaints received).
* In addition, seemingly silly information was kept - birthdays, family names, personal touches that allowed for a more intimate interaction whenever the custmoer was talking with you.
* We enabled easy integration to speed up various tasks.
  * Launch SSH/puTTY/Terminal/Remote Desktop to connect to a server.
    * Use SSH key OR password, if either are available.
    * Launch remote information collection, to gather current load/processes/other information that may be helpful either on call or to deal with a ticket
  * Auto-Dial desk phone or soft phone for contacting the customer
  * Build e-mail and ticket templates automatically using information contained overall
    * Includes monitoring statistics, bandwidth, any other information that we might deem necessary to satisfy the customer.

Even for things like the customer portal, CORE was absolutely necessary. CORE served both as a front end and an API backend. This was used extensively to supply the customer portal with the information for the accounts, usage, bandwidth, tickets and so on. Everything in the company was designed around this single platform.

As the company grew, so did its functionality. We added backup support, cataloging both the tape based long-term storage, and the short-term storage platforms in use. We allwoed approved employees access to the APIs to allow them to build their own productivity tools against the software, and some of these employees went on to extend CORE itself in doing so.

### Permissions

One of the particularly useful parts of CORE was by simply being the singular source of information, it enabled a much more streamlined newhire process. Permissions were handled entirely within CORE. User accounts were created within LDAP through CORE. Access to view, create, work, or close tickets; view, modify, delete account information; Queue permissions; Inventory permissions; Monitoring permissions. If you have used something like vBulletin before, CORE had a very similar, high-detail permissions system allowing for extremely accurate control of those permissions.

### Ticket Handling - The Main Part

CORE's primary function, as a ticketing system, was also its best.

**Queues:** One of the biggest problems I notice in ticketing systems is a lack of queues. Queues are important. They are meant to be numerous, and to be specific. CORE had hundreds of queues.
* Tiered Requests.
  * Support Queues *These are per **team** and not just 6 individual queues.*
    * New - The triage queue.
    * Level 1 - Easy issues, mostly questions and not actual problems.
    * Level 2 - Things like, restart apache, out of disk space, etc
    * Level 3 - System crashing, load issues, compromised, spam
    * Level 4 - Engineering, AUP violations, complex discussions, emergencies
    * Leads - Disgruntled customers, AUP violations, spam, fraud
  * Individual Engineering Queues
    * NOC (non-internal)
    * Backups/Storage
    * Products
      * Virtualization
      * Operating System Development
      * General Product Maintenance
  * Sales Queues *Sames as support, per **team** and not just for a single department.*
    * Incoming Sales
    * Winback/Churn
    * Business Development
    * QA (Lead Generation)
    * QA (Quality Assurance)
    * Triage
  * DCOPs Queues
    * Maintenance Queues
    * Inventory Queues
    * General Queues
  * AUP/TOS/Security/Abuse queues
  * Internal Queues
    * Helpdesk
    * Engineering/OPs
    * Inventory
    * Newhires & Departures
    * NOC
    * Security
  * Specialty Queues
    * Non-English Queues
    * Referral Partners

It might seem complicated, or otherwise counter-intuitive to have this many queues. If they were presented 100% of the time without limit, then yes - indeed it would be. However, there were systems in place to enable better usage of the system.

1. You can only see queues for which you are permitted to view.
2. Queues can be divided, combined, or isolated.
3. Every department was organized in a concise, structureed design.

For example, in support, you were a part of a team. That team had a specific list of customers that they handled, and only those customers. Each team had their own support queues. Each team had specific roles assigned to its members, depending on skill and responsibilities. As a result, the maximum number of queues an individual in support with the greatest overall responsibilities, would be 6 support queues + 2 internal engineering queues + 1 DC triage queue + Helpdesk. So, at most, 9 queues.

In addition, CORE made heavy use of automation. Since CORE had all of the information necessary for an account, it was possible to know where a ticket should go. Thus, CORE would take into account

* Location of machine(s) on ticket (DC)
* Type of ticket (Support, Sales, DCOPs)
* Urgency of ticket

Using the information provided it would move the ticket accordingly. As the available technologies improved, so did the automation. We began looking for keywords, and developed automated decision making, including full runbook automation.

**Response Times:** Early on, we started to notice that a primary reason customers would call in, be irate, or generally be impatient, was related to the overall time it took for any response. We surveyed customers, and found an overwhelming desire just simply to know that their ticket was being acknowledged. This was a critical eye opener, and was not fixed with just a single solution.

1. Using the previously mentioned automation, the system automatically responded to any new ticket that it had been filed to the appropriate support queue, that their support team had been notified of the new ticket, and that they can expect to receive a response with x amount of time.
  * The time was dynamic. This is because all technicians were required to log time no matter what, and we would measure the time for a human response since the last response in the ticket. In addition, as the system knew who was responsible for the ticket and who was logged in at the time of the ticket, the system was able to know whether or not there were available technicians or if it was a busy night. Using this, we would provide a relative estimate for when the next technician would be available.
2. The customers received an automated response for these moments in a ticket's lifetime
  * Initial Opening
  * Department Change (IE Support -> Backups)
  * Ticket Status
    * If Resolved, two messages
      * Courtesy note asking for the customer to take a satisfaction survey (covered later)
      * Courtesy note explaining the ticket will be permanently closed if no further action taken
    * If Resolved before issue is fixed
    * If closed by customer
    * If monitoring event (IE Ping Fail -> Ping Succeed)
  * Reprioritizing/Triaging

**Accountability:** CORE kept track of everything. This meant that if an employee read a ticket and then left it without an update, CORE knew. This accountability was transparent to the employee as well as available to the manager. It should be noted however that this information was only used to explain when a ticket went poorly - it was not normally a statistic used for judging technician quality.

**Statistics:** Along side the accountabilty, CORE kept a great number of statistics. This included the number of tickets handled all the way from the total tickets for the day, to the department, team, individual, etc. This information was mostly used to judge headcount, and was only referred to when employees were being reported for poor ticket satisfaction.

**Surveys:** This was one of the most important parts of the ticket, and was the line by which Rackspace set its requirements for an employee. Fanatical Support was determined strictly through a very simple, very well known rating system. The Net Promoter Score system has been used by all industries to judge the overall satisfaction levels of their customers. Indeed, it was very clear when the NPS went down, so did everything else - except churn, which then went up.

The survey was run in two forms. The first, was with every single ticket. You had the ability to choose a ranking of 0 to 5. We would use this as an internal estimation of satisfaction, as well as a key performance metric for employees. The second was sent out every 6 months to customers, and entailed more detailed questions, still under the same system however. In addition, any time a customer filled out the suggestions/complaint box with their ticket, it was immediately forwarded to the appropriate individuals - determined by the permissions within CORE.

**Information:** Finally, one of the most important portions of the ticket was the fact that it contained as much information related to the issue as possible. This was a great feature enabled by the overall integration that CORE had with the company.

1. The machine(s) affected. This is the most common item I see missed, or only partially included. CORE is the only system I have worked with that allowed for the attachment of multiple individual systems to the same ticket.
2. Automated CCs - The customer, account manager, and any other individual with permission to modify settings on an account, had the ability to set up automatic inclusions for the ticket. This allowed for multiple people being able to interact on the same ticket, so the need to go hunting for the right person was cut down significantly.
3. Linking - If issues were related, or if the problem was recurring, or if the ticket was a task in a much larger request, all of this could be bundled together so that the information is easily recalled and efficiently usable.
4. Extended information - If the issue is a support task, you can specify that. If it is a request for, say, new servers, you can specify that. You can include detailed information to improve search and visibility of the ticket in the queue.
  * Priority was important. The ability to identify a ticket as emergency, urgent, critical, medium, or low (not necessarily in that order). High-importancy tickets were given different colors in the queue.
  * SLAs were visible in the queue and within the ticket.
    * Popup alerts for tickets near their SLA were configurable.
    * Sounds were also available, though most didn't use them.
    * For high-importancy tickets, notifications to the techs and leadership staff were sent when SLA was being approached (or, if the customer replied, and an extended period went without our reply).
