#!/bin/bash
# This is an example script to get all datacenters using various methods.

read -p "Enter DCD Username: " pbuser
read -s -p "Enter DCD Password: " pbpass

echo "cUrl example:"
echo 'curl --user user:pass -X POST -h "Content-Type: text/xml" -H "SOAPAction: \"https://ws.api.profitbricks.com\"" --data-binary @xmlFile.xml https://api.profitbricks.com/1.3/apiFunctionCall'

curl --user $pbuser:$pbpass -X POST -H "Content-Type: text/xml" -H "SOAPAction: \"https://ws.api.profitbricks.com\"" --data-binary @getAllDataCenters.xml https://api.profitbricks.com/1.3/getAllDataCenters

echo "httpie example - httpie.org"
echo "httpie is a abstracted cUrl-like interface that allows for easier reading."

echo "http 
