#**<u>Profitbricks API demo suite</u>**

This repository will provide you with examples towards the usage of the Profitbricks public SOAP API as well as the private REST API that is available with the DCD and DCManager respectively.

___

## SOAP vs REST

Selecting an API to use is an important step in the development of an application around the profitbricks platform. As service representatives, it is advantageous and expected that we be able to provide assistance to new customers to our platform.

APIs enable the exposure of backend functionality to developers and integrators. In our case, this enables our platform's overall capabilities with larger companies to be truly utilized through automation and integration within their own platforms and applications.

At the heart of these systems, there are not many differences. They each provide the same service: a gateway to underlying functionality that allows for controlled operation and deployment of the platform that can be integrated into the customer's interfaces.

Web APIs are accessed through the usage of a formatted request. That request is generally in a format that is either in XML, JSON, or URL Encoded markup. The method for the request determines how we are able to interact with the service.

In summary, the differences between our two services are:

- The REST API is for the DCManager and can only be used by internal staff through the protected VPN. This API provides additional layers that are not normally available.
    - URL Encoded requests
    - Grants visibility into the physical layers of profitbricks (pservers, storage, networking)
    - Capable of Administrator level functionality
    - Authenticated by employee token from DCManager
- The SOAP API is for interactions with the DCD layer, or in other words the public facing interface.
    - XML based request
    - Only allows access to methods for handling functions available to an individual account
    - Uses Profitbricks login information

Now that we understand the overall situation, we can go over how to form a request and utilize the API tools.

### SOAP

A SOAP request looks like this:

    <?xml version="1.0"?>
    <soapenv:Envelope
      xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:ws="https://api.profitbricks.com/1.3/wsdl">
        <soapenv:Header>
        </soapenv:Header>
        <soapenv:Body>
            <ws:getDataCenter>
                <request>
                    <dataCenterId>1cf19941-7c0f-4632-a1ee-1cd1bfe485b2</dataCenterId>
                </request>
            </ws:getDataCenter>
        </soapenv:Body>
    </soapenv:Envelope>

The request is formed using XML markup, which is based off of the familiar HTML markup language used to design websites. The request is broken up into multiple sections.

- The Envelope
    - Defines the structure according to the SOAP standard schema (xmlns:soapenv)
    - Sets the web services location (xmlns:ws)
- The Header
    - Only necessary when required
    - Contains information about the request if the function requires it.
- The Body
    - Contains the function and any variables
    - Function in this demo: getDataCenter(), which returns the information for a given datacenter.
    - 